package com.hackaton.it.homeautomation;


import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.firebase.iid.FirebaseInstanceId;

import timber.log.Timber;

public class SmartApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        Timber.e("Created app:" +  FirebaseInstanceId.getInstance().getToken());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
