package com.hackaton.it.homeautomation.di;


import com.hackaton.it.homeautomation.network.CoreApi;
import com.hackaton.it.homeautomation.network.CoreInteractor;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
@Singleton
public class RestAdapterModule {

    @Provides
    @Singleton
    CoreInteractor interactor(CoreApi api) {
        return new CoreInteractor(api);
    }

    @Provides
    @Singleton
    Retrofit retrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://6080ed6b.ngrok.io")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().addInterceptor(
                        new Interceptor(){
                            @Override public Response intercept(Chain chain) throws IOException {
                                Request request = chain.request();
                                Timber.d(String.format("\nrequest:\n%s\nheaders:\n%s",
                                        request.body().toString(), request.headers()));
                                return chain.proceed(request);
                            }}).build())
                        .build();
    }


    @Provides
    @Singleton
    CoreApi eventApi(Retrofit retrofit) {
        return retrofit.create(CoreApi.class);
    }
}
