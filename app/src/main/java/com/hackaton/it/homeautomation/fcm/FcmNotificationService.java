package com.hackaton.it.homeautomation.fcm;


import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hackaton.it.homeautomation.R;

import timber.log.Timber;

public class FcmNotificationService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Timber.e("Token: " + FirebaseInstanceId.getInstance().getToken());

        Timber.e("Received extras: " + remoteMessage.getData().get("alert"));

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle("Message from our server").setContentText(remoteMessage.getData().get("alert"))
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
