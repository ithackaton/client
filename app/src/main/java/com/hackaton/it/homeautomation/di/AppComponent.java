package com.hackaton.it.homeautomation.di;


import com.hackaton.it.homeautomation.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {RestAdapterModule.class})
public interface AppComponent {
    void inject(MainActivity activity);

}
