package com.hackaton.it.homeautomation.fcm;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import timber.log.Timber;

public class FcmIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Timber.e("Token: " + FirebaseInstanceId.getInstance().getToken());
    }
}
